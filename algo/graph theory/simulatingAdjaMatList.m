function [adjazenzMat, adjazenzList] = simulatingAdjaMatList(element, k)

numOfElements              = length(element);
smallestEleVertices        = zeros(1, numOfElements);
for i=1:numOfElements
    temp                   = sort(element(i, :));
    smallestEleVertices(i) = temp(k + 1);                                   % k + 1 because of the null
end

adjazenzMat = zeros(numOfElements);
for i = 1:numOfElements
    for j = 1:numOfElements
        if i == j                                                           % i == j means if its null
           continue;
        end
        if element(i, j) <= smallestEleVertices(i)
           adjazenzMat(i, j) = element(i, j);
           adjazenzMat(j, i) = element(i, j);
        end
    end
end
adjazenzList = arrayfun(@(x) find(adjazenzMat(x, :) ~= 0), 1:numOfElements, 'UniformOutput', false)';    