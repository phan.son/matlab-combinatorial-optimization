%==========================================================================
% Author:         Phan Thanh Son
% E-Mail:         phan.thanh.son@outlook.de
% Last-Modified:  21.01.2019
% Version:        1.0
% Matlab-Version: R2019b
% The great circle distance or orthodromic distance is the shortest
% distance between two points on the surface of a sphere, measured along
% the surface of the sphere.
% Input:          angLongitudeA = angle in longitude from starting point
%                 angLatitudeA  = angle in latitude from starting point
%                 angLongitudeB = angle in longitude from ending point
%                 angLatitudeB  = angle in latitude from ending point
% Optional:       varargin      = the first four arguments in 'radian' or
%                                 'degree' anything else error
% Output:         d             = shortest distance between two points on
%                                 the surface of a sphere
% Source: https://de.wikipedia.org/wiki/Orthodrome
%==========================================================================

function d      = great_CircleDistance(angLongitudeA, angLatitudeA, angLongitudeB, angLatitudeB, radius, varargin)

C           = 2 * pi * radius;                                              % Circumference
if nargin == 6 
    if ~strcmp(varargin{1}, 'radian') && ~strcmp(varargin{1}, 'degree')
        error('Sixth argument is neither "degree" or "radian".');
    elseif strcmp(varargin{1}, 'radian')    
        zentriAngle = acos(sin(angLongitudeA) * sin(angLongitudeB) + cos(angLongitudeA) * cos(angLongitudeB) * cos(angLatitudeA -angLatitudeB));     
        d           = zentriAngle/(2 * pi) * C;
    elseif strcmp(varargin{1}, 'degree')
        zentriAngle = acosd(sind(angLongitudeA) * sind(angLongitudeB) + cosd(angLongitudeA) * cosd(angLongitudeB) * cosd(angLatitudeA -angLatitudeB));
        d           = zentriAngle/360 * C;
    end
elseif nargin > 6
    error('To many input arguments');
else
    zentriAngle = acosd(sind(angLongitudeA) * sind(angLongitudeB) + cosd(angLongitudeA) * cosd(angLongitudeB) * cosd(angLatitudeA -angLatitudeB));
    d           = zentriAngle/360 * C;
end
