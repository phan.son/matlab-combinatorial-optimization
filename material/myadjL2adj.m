function A = myadjL2adj( L )
   n = length(L);
   A = zeros( n, n );
   for v = 1:n
       for w = L{v}
           A(v,w) = 1;
           A(w,v) = 1;
       end
   end
end
