%==========================================================================
% Author:         Phan Thanh Son
% E-Mail:         phan.thanh.son@outlook.de
% Last-Modified:  21.01.2019
% Version:        1.0
% In mathematics, a spherical coordinate system is a coordinate system for 
% three-dimensional space where the position of a point is specified by 
% three numbers: the radial distance of that point from a fixed origin, 
% its polar angle measured from a fixed zenith direction, and the azimuthal 
% angle of its orthogonal projection on a reference plane that passes 
% through the origin and is orthogonal to the zenith, measured from a fixed 
% reference direction on that plane. It can be seen as the three-dimensional 
% version of the polar coordinate system.
% Input:          angLongitude = angle in longitude 
%                 angLatitude  = angle in latitude
%                 radius       = radius of sphere
% Optional:       varargin     = the first four arguments in 'radian' or
%                                'degree' anything else error
% Output:         p            = Coordinates (x, y, z)
% Source: https://en.wikipedia.org/wiki/Spherical_coordinate_system
%==========================================================================
function p = SphericalCoordinateSystem(angLongitude, angLatitude, radius, varargin)

if nargin == 3 
    degRad = 'degree';
elseif varargin{1} == 'degree'
    degRad = 'degree';
elseif varargin{1} == 'radian'
    degRad = 'radian';
elseif nargin > 3
    error('To many input arguments');
else 
    error('Sixth argument is neither "degree" or "radian".');
end
    
switch degRad
    case 'degree'
        x  = radius * sind(angLongitude) * cosd(angLatitude);
        y  = radius * sind(angLongitude) * sind(angLatitude);
        z  = radius * cosd(angLongitude);
        p  = [x, y, z];
    case 'radian'
        x  = radius * sin(angLongitude) * cos(angLatitude);
        y  = radius * sin(angLongitude) * sin(angLatitude);
        z  = radius * cos(angLongitude);
        p  = [x, y, z];
end
