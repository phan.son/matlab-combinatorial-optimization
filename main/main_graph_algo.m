%==========================================================================
% Author:         Phan Thanh Son
% E-Mail:         phan.thanh.son@outlook.de
% Last-Modified:  21.01.2019
% Version:        1.0
% Matlab-Version: R2019b
% Descripe here...
%==========================================================================

path    = 'C:\Users\Admin\Desktop\Projects\Matlab\Combinatorial optimization\material\Staedte.csv'; % Change here
p       = genpath('C:\Users\Admin\Desktop\Projects\Matlab\Combinatorial optimization'); % Change here
addpath(p);
city        = readcell(path);                                               % Index of 26 biggest city in Germany except Regensburg (55), City, degree of longitude, degree of latitude
clear path p;

numOfCity   = length(city);                                        

%% ===================Initialize distance matrix===========================
% =======Simplified modelling considering that the earth is a sphere=======
tic
fprintf('Initialize distance matrix...\n');
radiusEarth         = 6371.2;                                               % In km
distance            = zeros(numOfCity);

for i=1:numOfCity
    for j=1:numOfCity
        distance(i, j)  = great_CircleDistance(city{i, 3:4}, city{j, 3:4}, radiusEarth);
    end
end

distance            = real(distance);
distance(logical(eye(numOfCity))) = 0;                                      % Make sure eye of d is null;
fprintf('Done in %2f seconds.\n', toc);
%==========================================================================

%% ===============Simulating adjacency matrix/list==========================
% Simulating graph in which each city has a street to its nearest five
% neigbouring cities
% TODo: dont like the way this section was written, change later!
fprintf('Simulating adjacency matrix/list...\n');
k                          = 5;
[adjacencyMat, adjacencyList] = simulatingAdjaMatList(distance, k);
fprintf('Done in %2f seconds.\n', toc);
%==========================================================================

%% =====================Breadth First Search Algo==========================
fprintf('Breadth First Search Algorithm...\n');
% adjazenz matrix/list is an complete graph
s      = randi([1,numOfCity], 1);
[R, T] = breadthFirstSearch(adjacencyList, s);
fprintf('Done in %2f seconds.\n', toc);
%==========================================================================

%% =========================Dijkstra Algo==================================
fprintf('Dijkstra Algorithm...\n');
t      = randi(setdiff([1,numOfCity], s), 1);
[prev, path] = dijkstra(adjacencyList, distance, s, t);
fprintf('Done in %2f seconds.\n', toc);
%% ==========================Plot bfs algo=================================
%TODO FIX 
fprintf('Plot results...\n');
bfsGraph = digraph(T(:,1), T(:,2));
bfsGraph.Nodes.Name = city(:, 2);
plot(bfsGraph);

%==========================================================================
%% ==========================Plot Dijkstra=================================
plotDijk = graph(path)
%==========================================================================
%% ========================Plot adjazenz matrix============================
 plotGraph = graph(adjacencyMat);
 f = figure;
 plotGraph.Nodes.Name = city(:, 2);
 plotGraph.Edges.Weight = unique(adjacencyMat(find(adjacencyMat ~= 0)));      % TODO there is something wrong
 plot(plotGraph);
 title('Adjazenz matrix as graph');
%  plot(plotGraph,'EdgeLabel',plotGraph.Edges.Weight);
fprintf('Done in %2f seconds.\n', toc);