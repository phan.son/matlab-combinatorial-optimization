%==========================================================================
% Author:         Phan Thanh Son
% E-Mail:         phan.thanh.son@outlook.de
% Last-Modified:  21.01.2019
% Version:        1.0
% Matlab-Version: R2019b
% Calculate the euclidean distance 
% Input           p := x,y-Coordinates, starting point
%                 q := x,y-Coordinates, ending point
% Output:         d := euclidean Distance  
%==========================================================================
function d = euclideanDistance(p, q, varargin)

if nargin == 3
    if varargin{1} == 3
        d          = sqrt((p(1) - q(1))^2 + (p(2) - q(2))^2 + (p(1) - q(1))^2);
    elseif varargin{1} ~= 2 || varargin ~= 3
        error('Third argument must be 2 or 3');
    end
else
    d              = sqrt((p(1) - q(1))^2 + (p(2) - q(2))^2);
end
