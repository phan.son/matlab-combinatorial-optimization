%==========================================================================
% Author:         Phan Thanh Son
% E-Mail:         phan.thanh.son@outlook.de
% Last-Modified:  23.01.2019
% Version:        1.0
% Matlab-Version: R2019b
% There are better ways to solve this problem, Matlab already got some
% functions.
% Time is O(|V| + |E|)
% System First in first out
% Input:  adjacencyList  := adjacency list
%         s              := starting vertices
% Output: R              := all reachable vertices from starting node s
%         T              := corresponding connecting edge
% Source: https://en.wikipedia.org/wiki/Breadth-first_search
% ...\Matlab\Combinatorial optimization\doc\Volesung\Kombinatorische Optimierung
%==========================================================================
function [R, T] = breadthFirstSearch(adjacencyList, s)

%% ==============See Paper: Kombinatorische Optimierung====================
%=========================Algorithmus 2.36=================================

Q                       = s;                                                
R                       = s;
T                   	= [];
k                   	= 1;
list                    = adjacencyList;

while ~isempty(Q)
    v                   = Q(1);
    w                   = find(cellfun(@(x) sum(x==v), list) ==1);
    if isempty(w)
        Q               = setdiff(Q, v);
    else
        w               = w(1);                                             % First in first out
        list{w}(find(list{w} == v)) = [];
        T(k, :)         = [v, w];
        temp            = T(find(T ~= v));
        R               = union(R, temp, 'stable');
        Q               = union(Q, temp, 'stable');   
        k               = k + 1;
    end
end

    
