%==========================================================================
% Author:         Phan Thanh Son
% E-Mail:         phan.thanh.son@outlook.de
% Last-Modified:  26.01.2019
% Version:        1.0
% Matlab-Version: R2019b
% Runnng time is O(|V|^2*log|V|)
% Dijkstra algorithm only for postiv weights!
% Input:  adjacencyList  := adjacency list
%         weight         := weight matrix
%         start          := starting vertices
%         destination    := destination
%         Optional    'short' for shortest path
% Output: prev           := searches for the vertex u in the vertex set Q 
%                           that has the least dist(u) value
%         Optional       := shortest path
% Source: https://en.wikipedia.org/wiki/Dijkstra%27s_algorithm
% https://www.youtube.com/watch?time_continue=7&v=2poq1Pt32oE&feature=emb_logo
% ...\Matlab\Combinatorial optimization\doc\Volesung\Kombinatorische Optimierung
% Make sure weight is an upper triangular matrix or symmetric matrix if its
% a lower triangular matrix mirror it tot the diagonal
%==========================================================================zieldest
function [prev, varargout] = dijkstra(adjacencyList, weight, start, varargin)

%% ==============See Paper: Kombinatorische Optimierung====================
%=========================Algorithmus 2.49=================================
%% ============================Initialize==================================
if nargin == 3
    warning('Algorithm only returns the start node specifies the node from which the shortest routes to all nodes are searched.');
    destination = NaN;
else
   destination = varargin{1}; 
end

distance              = inf([2, length(adjacencyList)]);
distance([1, 2], start) = 0;
prev                 = [];
Q                    = 1:length(adjacencyList);

while ~isempty(Q)
    [~, u]           = min(distance(2,:));
    Q(Q == u)        = [];
    distance(2, u)    = inf;
    v                = intersect(adjacencyList{u}, Q); 
    if u == destination & nargin == 4
        [path]       = shortestPath(destination, prev);
        varargout{1} = path;
        break;
    else
        for i=1:length(v)
            [distance, prev] = update_weight(distance, weight, u, v(i), prev);
        end
    end
end

    function [distance, prev] = update_weight(distance, weight, u, v, prev)
        temp    = distance(1, u) + weight(u, v);
        if temp < distance(1, v)
            distance([1, 2], v) = temp;
            prev(v)      = u;
        else
            distance     = distance;
            prev         = prev;
        end
    end

    function [path] = shortestPath(destination, prev)
        path     = destination;
        vertex   = destination;
        k        = 2;
        while prev(vertex) ~= 0
            vertex    = prev(vertex);
            path(k)     = vertex;
            k           = k + 1;
        end
        path            = flip(path);
    end

end
